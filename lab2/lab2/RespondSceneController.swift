//
//  RespondSceneController.swift
//  lab2
//
//  Created by martine on 13/04/2020.
//  Copyright © 2020 charles. All rights reserved.
//

import UIKit

class RespondSceneController: UIViewController, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    @IBOutlet var labelText: UILabel!
    @IBOutlet var newTextField: UITextField!
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer){
        newTextField.resignFirstResponder()
    }
    
    @IBAction func changeText(_ sender: UIButton) {
        if let answer = newTextField.text {
            if (answer == "") {
                labelText.text = "Label"
            } else {
            labelText.text = answer;
            }
        }
    }
}
