//
//  ViewController.swift
//  lab2
//
//  Created by martine on 05/04/2020.
//  Copyright © 2020 charles. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let existingTextHasDecimalSeparator = textField.text?.range(of: ".")
        let replacementTextHasDecimalSeparator = string.range(of: ".")
        if existingTextHasDecimalSeparator != nil,
            replacementTextHasDecimalSeparator != nil {
            return false
        }
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) == nil,
            !string.contains("."), !string.isEmpty {
        return false
    }
    return true
}

    
    @IBOutlet var centimetersLabel: UILabel!
    
    @IBAction func inchFieldEditingChanged(_ textField: UITextField) {
        if let text = textField.text, let value = Double(text){
            celsiusValue = Measurement (value: value, unit: .celsius)
        } else{
            celsiusValue = nil
        }
    }
     
    @IBOutlet var inchesTextField: UITextField!
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer){
        inchesTextField.resignFirstResponder()
    }
    
    var celsiusValue: Measurement <UnitTemperature >?{
    didSet {
    updateCentimetersLabel()
        }
    }
    var fahrenheitValue: Measurement <UnitTemperature >? {
        if let celsiusValue =  celsiusValue {
            return celsiusValue.converted(to: .fahrenheit)
        } else {
            return nil
        }
    }
    
    func updateCentimetersLabel() {
        if let fahrenheitValue = fahrenheitValue {
            centimetersLabel.text = numberFormatter.string(from: NSNumber(value: fahrenheitValue.value))
        } else {
            centimetersLabel.text = "???" }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateCentimetersLabel()
    }
    
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 2
        return nf
        
    }()
}

