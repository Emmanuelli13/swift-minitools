//
//  CalculatorSceneController.swift
//  lab2
//
//  Created by martine on 13/04/2020.
//  Copyright © 2020 charles. All rights reserved.
//

import UIKit

class CalculatorSceneController: UIViewController, UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    @IBAction func field1EditingChanged(_ textField: UITextField) {
        operation()
    }
    
    @IBAction func field2EditingChanged(_ textField: UITextField) {
        operation()
    }
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer){
        value1.resignFirstResponder()
        value2.resignFirstResponder()
    }
    
    @IBOutlet weak var value1: UITextField!
    @IBOutlet weak var value2: UITextField!
    @IBOutlet weak var sum: UILabel!
    @IBOutlet weak var sub: UILabel!
    @IBOutlet weak var mult: UILabel!
    @IBOutlet weak var div: UILabel!
    @IBOutlet weak var parity: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        operation()
    }
    
    func operation(){
        let number1: Int?
        let number2: Int?
        
        if let text = value1.text {
            number1 = Int(text)
        } else{
            number1 = nil
        }
        if let text = value2.text {
            number2 = Int(text)
        } else{
            number2 = nil
        }
        
        if (number1 != nil && number2 == nil) {
            let num1: Int = number1!
            sum.text = "\(String(describing: num1))"
            sub.text = "\(String(describing: num1))"
            mult.text = "0"
            div.text = "nil"
            if (num1 % 2 == 0){
                parity.text = "yes,yes"
            } else {
                parity.text = "no,yes"
            }
        }
        else if (number1 == nil && number2 == nil) {
            sum.text = "sum result"
            sub.text = "subs result"
            mult.text = "mult result"
            div.text = "div result"
            parity.text = "parity (yes/no)"
        }
        else if (number1 == nil && number2 != nil) {
            let num2: Int = number2!
            sum.text = "\(String(describing: num2))"
            sub.text = "\(String(describing: 0 - num2))"
            mult.text = "0"
            div.text = "0"
            if (num2 % 2 == 0){
                parity.text = "yes,yes"
            } else {
                parity.text = "yes,no"
            }
        }
       
        else if (number1 != nil && number2 != nil) {
            let num1: Int = number1!
            let num2: Int = number2!
            sum.text = "\(String(describing: num1 + num2))"
            sub.text = "\(String(describing: num1 - num2))"
            mult.text = "\(String(describing: num1 * num2))"
            div.text = "\(String(describing: num1 / num2))"
            if (num1 % 2 == 0 && num2 % 2 == 0){
                parity.text = "yes,yes"
            } else if(num1 % 2 != 0 && num2 % 2 == 0) {
                parity.text = "no,yes"
            } else if (num1 % 2 == 0 && num2 % 2 != 0){
                parity.text = "yes,no"
            }
            else if (num1 % 2 != 0 && num2 % 2 != 0){
                parity.text = "no,no"
            }
        }
    }
}
